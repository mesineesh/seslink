package SESLinkPack;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SESLinkTask 
{
    @Test
    public void test () throws InterruptedException{
            System.setProperty("webdriver.chrome.driver", "C:\\Auto\\Chrome\\2\\chromedriver.exe");
            WebDriver Dr = new ChromeDriver();
            Dr.manage().window().maximize();
            Dr.get("https://link.springer.com/");
            Thread.sleep(2000);
            WebElement ele = Dr.findElement(By.xpath("//*[@id=\"query\"]")); 
            ele.sendKeys("History");
            ele.sendKeys(Keys.ENTER); 
            Thread.sleep(2000);
            String result= Dr.findElement(By.xpath("//*[@id=\"results-list\"]/li[1]/h2/a")).getText();
            Assert.assertEquals("History", result);

      }
}